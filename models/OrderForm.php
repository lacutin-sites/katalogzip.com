<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class OrderForm extends Model
{
    public $name;
    public $email;
    public $phone;
    public $message;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['name', 'phone'], 'required'],
            // email has to be a valid email address
            ['email', 'email'],


        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Имя',
            'phone' => 'Телефон',
            'email' => 'Почта',
            'message' => 'Сообщения',
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @param string $email the target email address
     * @return bool whether the model passes validation
     */
    public function contact($email)
    {
        if ($this->validate()) {

            Yii::$app->mailer->compose()
                ->setTo($email)
                ->setFrom([Yii::$app->params['senderEmail'] => Yii::$app->params['senderName']])
                ->setReplyTo([$this->email => $this->name])
                ->setSubject('Заказ от клиента')
                ->setTextBody($this->message.'Идентификаторы товаров: '.implode("|",Yii::$app->session->get('cart'))
                    ."\r\nФИО Клиента: {$this->name}\r\nПочта клиента :".$this->email."\r\nТелефон клиента :".$this->phone)
                ->send();


            return true;
        }
        return false;
    }
}
