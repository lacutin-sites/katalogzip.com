<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\Url;
AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="yandex-verification" content="a3a596e48d2d24dd" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="/img/logoico.png" type="image/png">
    <meta name="yandex-verification" content="ae21aef204d34e6c" />
    <meta name="google-site-verification" content="XadDYIqH0U-QKELE3KeSZf5MvjuZiEe7khLY84S8eJA" />
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<header class="header_area">
    <div class="top_menu">
        <div class="container">
            <div class="row">
                <div class="col-lg-7">
                    <div class="float-left">
                        <p>Телефон: 8 (800) 201-30-78</p>
                        <p>Почта: info@katalogzip.com</p>
                    </div>
                </div>
                <div class="col-lg-5">
                    <div class="float-right">
                        <ul class="right_side">

                            <li>
                                <a href="<?=Url::to(['site/contact']);?>">
                                    Связаться с нами
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="main_menu">
        <div class="container">
            <nav class="navbar navbar-expand-lg navbar-light w-100">

                <a class="navbar-brand logo_h" href="/">
                    <img src="/img/logo.png" alt="Katalogzip.com" />
                    <h3 style='color:#e67817'>8 (800) 201-30-78</h3>
                </a>

                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse offset w-100" id="navbarSupportedContent">
                    <div class="row w-100 mr-0">
                        <div class="col-lg-7 pr-0">
                                
                            <ul class="nav navbar-nav center_nav pull-right">
                                <li class="nav-item ">
                                    <a class="nav-link" href="/">Домой</a>
                                </li>
                                <li class="nav-item submenu dropdown">
                                    <a href="<?=Url::to(['category/index/'])?>" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                                       aria-expanded="false">Каталог</a>
                                    <ul class="dropdown-menu">
                                        <?php
                                        foreach (\app\models\Category::find()->all() as $catItem) {
                                            ?>
                                            <li class="nav-item">
                                                <a class="nav-link" href="<?=Url::to(['category/index/','CategorySearch[id]'=>$catItem->id]);?>"><?= $catItem->name ?></a>

                                            </li>
                                            <?php
                                        }
                                        ?>

                                    </ul>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="<?=Url::to(['site/contact']);?>">Контакты</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link"  href="<?=Url::to(['site/about']);?>">
                                        О Нас
                                    </a>
                                </li>
                            </ul>
                        </div>

                        <div class="col-lg-5 pr-0">
                            <ul class="nav navbar-nav navbar-right right_nav pull-right">
                                <li class="nav-item">
                                    <a href="<?=Url::to(['category/index/']);?>" class="icons">
                                        <i class="ti-search" aria-hidden="true"></i>
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a  href="<?=Url::to(['site/cart']);?>" class="icons">
                                        <i class="ti-shopping-cart"> <sup><?=count(Yii::$app->session->get('cart'))?></sup></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
        </div>
    </div>
</header>
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            'homeLink' => ['label' => 'Домой',
                'url' => '/' ],
        ]) ?>
        <?= Alert::widget() ?>
<?= $content ?>

<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

   ym(54258391, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
   });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/54258391" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->


<footer class="footer-area section_gap">
    <div class="container">
        <div class="row justify-content-center">
            <div class="main_title">
                <h2><span>Наши филиалы</span></h2>

            </div>
        </div>
        
            <div class="row">
                <div class="col-lg col-md-6 single-footer-widget">
                    <h4>г.Новосибирск</h4>
                    <ul>
                        <li>
                            Адрес: 630028 г.Новосибирск,ул.Чехова,421 оф.22.
                        </li>
                    </ul>
                </div>
                <div class="col-lg col-md-6 single-footer-widget">
                    <h4 >г. Омск</h4>
                </div>
                <div class="col-lg col-md-6 single-footer-widget">
                    <h4>г. Тюмень</h4>
                </div>
                <div class="col-lg  single-footer-widget">
                    <h4>г. Кемерово</h4>
                </div>
                <div class="col-lg single-footer-widget">
                    <h4>г. Красноярск</h4>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col">
                    <h4 class='text-right'><span>Единый телефон для России <a href="tel:8 (800) 201-30-78">8 (800) 201-30-78</a></span></h4>
                    <h4 class='text-right'><span>Почта: <a href="mailto:info@katalogzip.com">info@katalogzip.com</a></span></h4>
                </div>
            </div>
            <div class="footer-bottom row align-items-center">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="text-black col">
                                            ООО «Кварц Энерджи»
                                        </div>
                                        <div class="text-black col">
                                            ИНН 5405025010
                                        </div>
                                        <div class="text-black col">
                                            ОГРН 1185476055104
                                        </div>
                                        <div class="text-black col">
                                            Все права защищены 2019
                                            <a href="https://colorlib.com" target="_blank">Colorlib</a>
                                        </div>
                                    </div>
                                </div>

            </div>

</footer>
<script src="//code.jivosite.com/widget.js" data-jv-id="8t77oyE6GK" async></script>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
