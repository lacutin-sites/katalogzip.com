<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\GoodsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="container-fluid">

    <?php $form = ActiveForm::begin([
        'action' => ['good/index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1,
            'class'=>'form-contact form contact_form'
        ],
    ]); ?>

    <?= $form->field($model, 'id')->textInput(['maxlength' => 255, 'class' => 'form-control'])->label('Поис по идентификатору') ?> 

    <?= $form->field($model, 'title')->textInput(['maxlength' => 255, 'class' => 'form-control'])->label('Поис по названию') ?> 

    <?= $form->field($model, 'about')->textInput(['maxlength' => 255, 'class' => 'form-control'])->label('Поис по описания') ?> 

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Поиск'), ['class' => 'main_btn']) ?>
        <?= Html::resetButton(Yii::t('app', 'Сбросить'), ['class' => 'main_btn']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
