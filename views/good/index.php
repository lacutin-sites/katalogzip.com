<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\GoodsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Каталог');
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="cat_product_area section_gap">
    <div class="container">
        <div class="row flex-row-reverse">
            <div class="col-lg-9">
                <div class="product_top_bar">
                    <div class="left_dorp">
                        <?php  echo $this->render('/good/_search', ['model' => $searchModel]);?>
                    </div>
                </div>

                <div class="latest_product_inner">
                    <div class="row">


<?php
      foreach ($dataProvider->getModels() as $good) {
                                ?>
                                <div class="col-lg-4 col-md-6 ">
                                    <div class="single-product">
                                        <div class="product-img">
                                            <img
                                                    class="card-img"
                                                    src="/content/<?= $good->id ?>"
                                                    alt=""
                                            />
                                            <div class="p_icon">
                                                <a href="<?= Url::to(['good/view/', 'id' => $good->id]); ?>">
                                                    <i class="ti-eye"></i>
                                                </a>
                                                <a onclick="addToCart(<?= $good->id ?>)">
                                                    <i class="ti-shopping-cart"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="product-btm">
                                            <a href="<?= Url::to(['good/view/', 'id' => $good->id]); ?>"
                                               class="d-block">
                                                <h4><?= $good->title ?></h4>
                                            </a>
                                            <div class="mt-3">
                                                <span class="mr-4">Цена: <strong>По запросу</strong></span>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <?php
                            }

                                echo \yii\widgets\LinkPager::widget([
                                    'pagination' => $dataProvider->pagination,
                                ]);

                            ?>
                    </div>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="left_sidebar_area">
                    <aside class="left_widgets p_filter_widgets">
                        <div class="l_w_title">
                            <h3>Каталог</h3>
                        </div>
                        <div class="widgets_inner">
                            <ul class="list">
                                <?php


                                foreach (\app\models\Category::find()->all() as $cat) {

                                    ?>
                                    <li >
                                        <a href="<?=Url::to(['category/index/','CategorySearch[id]'=>$cat->id]);?>"><?=$cat->name?></a>
                                    </li>
                                    <?php
                                }
                                ?>

                            </ul>
                        </div>
                    </aside>



                </div>
            </div>
        </div>
    </div>
</section>


