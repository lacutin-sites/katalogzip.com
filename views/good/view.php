<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Goods */

$page=\app\models\Pages::find()->where(['id'=>$model->id])->all();
$this->title=$page[0]->title;

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Каталог'), 'url' => ['index']];
$this->params['breadcrumbs'][] = \app\models\Category::find()->where(['id'=>$model->category_id])->all()[0]->name;


$this->params['breadcrumbs'][] = $model->title;
\yii\web\YiiAsset::register($this);
\Yii::$app->view->registerMetaTag([
            'name' => 'description',
            'content' => $page[0]->description
        ]);

\Yii::$app->view->registerMetaTag([
    'name' => 'keywords',
    'content' => $page[0]->keyword
]);

\Yii::$app->view->registerMetaTag([
    'name' => 'og:title',
    'content' => $page[0]->title
]);
\Yii::$app->view->registerMetaTag([
    'name' => 'og:image',
    'content' => '/content/'.$model->id
]);
\Yii::$app->view->registerMetaTag([
    'name' => 'og:url',
    'content' =>  URL::to()
]);
\Yii::$app->view->registerMetaTag([
    'name' => 'og:description',
    'content' =>  $page[0]->description
]);


Yii::$app->view->registerLinkTag(['rel' => 'canonical', 'href' => URL::to()]);




?>


<!--================Single Product Area =================-->
<div class="product_image_area pb-5" >
    <div class="container">
        <div class="row s_product_inner">
            <div class="col-lg-6">
                <div class="s_product_img">
                    <div
                            id="carouselExampleIndicators"
                            class="carousel slide"
                            data-ride="carousel"
                    >
                        <ol class="carousel-indicators">

                        </ol>
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img
                                        class="d-block img-fluid"
                                        src="/content/<?=$model->id?>"
                                        alt="<?=$model->title?>"
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 offset-lg-1">
                <div class="s_product_text">
                    <h3><?=$model->title?></h3>
                    <h2>Цена : По запросу </h2>
                    <ul class="list">
                        <li>
                            <a class="active"  href="<?=Url::to(['category/index/','CategorySearch[id]'=>$model->category_id]);?>">
                                <span>Категория</span> : <?=\app\models\Category::find()->where(['id'=>$model->category_id ])->one()->name?></a
                            >
                        </li>

                    </ul>
                    <p>
                        <?=strip_tags($model->about,'<p>,<a>,<br>')?>

                    </p>

                    <div class="card_area">
                        <a class="main_btn" onclick="addToCart(<?=$model->id?>)">В корзину</a>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
