<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;


$this->title = 'Карзина покупок';
$this->params['breadcrumbs'][] = $this->title;



?>
<section class="cart_area">
    <div class="container ">
        <div class="cart_inner">
            <?php
            if (count($cart)>0) {

                ?>
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">Продукт</th>
                        <th scope="col">Цена</th>
                        <th scope="col">Удалить</th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php

                    foreach ($cart

                    as $item){
                    ?>
                    <tr>
                        <td>
                            <div class="media1">
                                <div class="text-left">
                                    <img width="100" height="100"
                                         src="/content/<?= $item->id ?>"
                                         alt=""
                                    />
                                </div>
                                <div class="media-body">
                                    <p><?= $item->title ?></p>
                                </div>
                            </div>
                        </td>
                        <td class="text-center">
                            <h5>По запросу</h5>
                        </td>
                        <td class="text-center">
                            <h5><a href="" onclick="removeFromCart(<?= $item->id ?>)">
                                    <i class="fa fa-close"></i></a>
                            </h5>
                        </td>

                        <?php

                        }
                        ?>

                    </tr>
                    <tr class="out_button_area">
                        <td></td>
                        <td>
                            <div class="checkout_btn_inner">
                                <a class="gray_btn" href="/">Продолжить покупку</a>
                                <a class="main_btn" href="/order">Заказать</a>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <?php
            }else{
                echo  'Карта пуста<br><a class="gray_btn" href="/">Продолжить покупку</a>';
            }
            ?>


            </div>
        </div>
    </div>
</section>

