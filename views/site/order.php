<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\OrderForm */
/* @var $form ActiveForm */
?>

<section class="section_gap">
    <div class="container">
        <div class=" mb-5 pb-4">

        <div class="row">
            <div class="col-12">
                <h2 class="contact-title">Заказать</h2>
            </div>
            <div class="col-lg-8 mb-4 mb-lg-0">
                  <?php if (Yii::$app->session->hasFlash('orderFormSubmitted')): ?>
                      <div class="row">
                            <div class="col">
                                <div class="alert alert-success">
                                    Cпасибо за ваш заказа. Мы ответим вам как можно скорее.
                                </div>

                            </div>
                      </div>
                        <?php else: ?>
                        <?php $form = ActiveForm::begin([ 'options' => [
                          'class'=>'form-contact contact_form'
                      ]]); ?>
                      <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                        <?= $form->field($model, 'name')->textInput(['autofocus' => true, 'class'=>"form-control",'placeholder'=>'Имя'])->label('Имя') ?>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                        <?= $form->field($model, 'phone')->textInput(['autofocus' => true, 'class'=>"form-control",'placeholder'=>'Номер телефона'])->label('Номер телефона') ?>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                        <?= $form->field($model, 'email')->textInput(['autofocus' => true, 'class'=>"form-control",'placeholder'=>'Почта'])->label('Почта') ?>
                        </div>
                    </div>
                    <div class="col-12">
                            <div class="form-group ">
                                <?= Html::submitButton('Заказать', ['class' => 'main_btn', 'name' => 'contact-button']) ?>
                            </div>
                        </div>
                    <?php ActiveForm::end(); ?>
                <?php endif; ?>

        </div>
        </div>
     </div>

    </section>

