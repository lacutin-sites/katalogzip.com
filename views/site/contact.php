<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Контакты';
$this->params['breadcrumbs'][] = $this->title;
?>

<section class="section_gap">
    <div class="container">
        <div class="mb-5 pb-4">

        <div class="row">
            <div class="col-12">
                <h2 class="contact-title">Связаться</h2>
            </div>
            <div class="col-lg-8 mb-4 mb-lg-0">
                  <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>
                      <div class="row">
                            <div class="col">
                                <div class="alert alert-success">
                                    Благодарим Вас за обращение к нам. Мы ответим вам как можно скорее.
                                </div>

                            </div>
                      </div>
                        <?php else: ?>
                        <?php $form = ActiveForm::begin(['id' => 'contactForm', 'options' => [
                          'class'=>'form-contact contact_form'
                      ]]); ?>
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">

                        <?= $form->field($model, 'name')->textInput(['autofocus' => true, 'class'=>"form-control",'placeholder'=>'Имя'])->label('Имя') ?>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="form-group">
                        <?= $form->field($model, 'email')->textInput(['autofocus' => true, 'class'=>"form-control",'placeholder'=>'Почта'])->label('Почта') ?>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                        <?= $form->field($model, 'subject') ->textInput(['autofocus' => true, 'class'=>"form-control",'placeholder'=>'Тема письма'])->label('Тема письма') ?>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">

                        <?= $form->field($model, 'body')->textarea(['rows' => 6, 'class'=>"form-control w-100",'placeholder'=>'Сообщения'])->label('Сообщения') ?>
                        </div>
                    </div>

                    <div class="col-12">
                            <div class="form-group ">
                                <?= Html::submitButton('Заказать', ['class' => 'main_btn', 'name' => 'contact-button']) ?>
                            </div>
                        </div>
                </div>

                        <?php ActiveForm::end(); ?>

                <?php endif; ?>



            </div>

            <div class="col-lg-4">
                <div class="media contact-info">
                    <span class="contact-info__icon"><i class="ti-tablet"></i></span>
                    <div class="media-body">
                        <h3><a href="tel: 8 (800) 201-30-78"> 8 (800) 201-30-78</a></h3>
                        <p>пн-сб с 9 до 20</p>
                    </div>
                </div>
                <div class="media contact-info">
                    <span class="contact-info__icon"><i class="ti-email"></i></span>
                    <div class="media-body">
                        <h3><a href="mailto:support@colorlib.com">info@katalogzip.com</a></h3>
                        <p>Отправьте нам свой запрос в любое время!</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>




