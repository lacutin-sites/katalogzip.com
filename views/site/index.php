<?php

/* @var $this yii\web\View */

$this->title = 'Домой';

use yii\helpers\Url;
?>
<section class="home_banner_area ">
    <div class="banner_inner d-flex align-items-center">
        <div class="container">
            <div class="banner_content row">
                <div class="col-lg-12">
                    <a class="main_btn mt-40" href="<?=Url::to(['good/index'])?>">Посмотреть каталог</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!--================End Home Banner Area =================-->

<!-- Start feature Area -->
<section class="feature-area section_gap_bottom_custom pt-0">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="main_title">
                    <h2><span>Котегории продукции</span></h2>

                </div>
            </div>
        </div>
        <div class="row">
            <?php foreach ($category as $ct){?>
            <div class="col-lg-3 col-md-6">
                <div class="single-feature">
                    <a href="<?=Url::to(['category/index/','CategorySearch[id]'=>$ct->id]);?>" class="title">
                        <img src="<?=$ct->image?>" height="90" width="90"  alt="<?=$ct->name?>">
                        <h3 class="text-center"> Оборудование  <?=$ct->name?> </h3>
                    </a>

                </div>
            </div>
            <?php } ?>

        </div>
    </div>
</section>
<!-- End feature Area -->

<!--================ Feature Product Area =================-->
<section class="feature_product_area section_gap_bottom_custom">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="main_title">
                    <h2><span>Новые продукты</span></h2>
                    <p>Наш ассортимент постоянно пополняется новыми товарами от ведущих производителей</p>
                </div>
            </div>
        </div>

        <div class="row">
            <?php
                $i=0;
                foreach ($goods as $good) {
                    if ($i<3) {
                        ?>

                        <div class="col-lg-4 col-md-6">
                            <div class="single-product border-info">
                                <div class="product-img ">
                                    <img  width="200" height="200" src="/content/<?=$good->id?>" alt="<?=$good->title?>"/>
                                    <div class="p_icon">
                                        <a href="<?=Url::to(['good/view/','id'=>$good->id]);?>">
                                            <i class="ti-eye"></i>
                                        </a>
                                        <a  onclick="addToCart(<?=$good->id?>)">
                                            <i class="ti-shopping-cart"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="product-btm">
                                    <a href="<?=Url::to(['good/view/','id'=>$good->id]);?>" class="d-block">
                                        <h4><?=$good->title?></h4>
                                    </a>
                                    <div class="mt-3">
                                        <span class="mr-4">Цена: <strong>По запросу</strong></span>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    $i++;
            }
            ?>

        </div>
    </div>
</section>
<!--================ End Feature Product Area =================-->

<!--================ Offer Area =================-->
<section >
    <!--
    <div class="container">
        <div class="row justify-content-center">
            <div class="offset-lg-4 col-lg-6 text-center">
                <div class="offer_content">
                    <h3 class="text-uppercase mb-40">all men’s collection</h3>
                    <h2 class="text-uppercase">50% off</h2>
                    <a href="#" class="main_btn mb-20 mt-5">Discover Now</a>
                    <p>Limited Time Offer</p>
                </div>
            </div>
        </div>
    </div>
    !-->
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="carousel slide" id="carousel-765102">
                    <ol class="carousel-indicators">
                        <li data-slide-to="0" data-target="#carousel-765102" class="active">
                        </li>
                        <li data-slide-to="1" data-target="#carousel-765102" >
                        </li>
                        <li data-slide-to="2" data-target="#carousel-765102" >
                        </li>
                        <li data-slide-to="3" data-target="#carousel-765102" >
                        </li>

                    </ol>
                    <div class="carousel-inner">
                        <div class="carousel-item active carousel-item-left carousel-item">
                            <img class="d-block w-100" alt="SIEMENS" src="/img/banner/1.jpg" />

                        </div>
                        <div class="carousel-item carousel-item-next carousel-item-left  carousel-item">
                            <img class="d-block w-100" alt="DUNGS" src="/img/banner/2.jpg" />
                        </div>
                        <div class="carousel-item carousel-item-next carousel-item-left  carousel-item">
                            <img class="d-block w-100" alt="SUNTEC" src="/img/banner/3.jpg" />

                        </div>
                        <div class="carousel-item carousel-item-next carousel-item-left  carousel-item">
                            <img class="d-block w-100" alt="AUTOFLAME" src="/img/banner/4.jpg" />

                        </div>


                </div>
            </div>
        </div>
    </div>
</section>


<section class="inspired_product_area section_gap_bottom_custom pt-3">
    <div class="container pt-5">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="main_title">
                    <h2><span>В топе</span></h2>

                </div>
            </div>
        </div>

        <div class="row">
            <?php

            foreach ($category as $cat) {
                ?>

                <div class="col-lg-3 col-md-6 ">
                    <div class="single-product border border-info">
                        <div class="product-img">
                            <img width="200" height="200" src="<?=$cat->image ?>"
                                 alt="<?= $cat->name ?>"/>
                            <div class="p_icon">
                                <a href="<?=Url::to(['category/index','CategorySearch[id]'=>$cat->id]);?>">
                                    <i class="ti-eye"></i>
                                </a>
                            </div>
                        </div>
                        <div class="product-btm">
                            <a  href="<?=Url::to(['category/index','CategorySearch[id]'=>$cat->id]);?>" class="d-block">

                                <h4>Оборудования <?= $cat->name ?></h4>
                            </a>

                        </div>
                    </div>
                </div>

            <?php    foreach (\app\models\Goods::find()->where(['category_id'=>$cat->id])->orderBy('LENGTH(title),rand()')->limit(3)->all() as $good)
            {

                    ?>
                    <div class="col-lg-3 col-md-6 p-2">
                        <div class="single-product ">
                            <div class="product-img">
                                <img width="150" height="150" src="/content/<?= $good->id ?>"
                                     alt="<?= $good->title ?>"/>
                                <div class="p_icon">
                                    <a href="<?=Url::to(['good/view/','id'=>$good->id]);?>">
                                        <i class="ti-eye"></i>
                                    </a>
                                    <a  onclick="addToCart(<?=$good->id?>)">
                                        <i class="ti-shopping-cart"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="product-btm">
                                <a href="<?=Url::to(['good/view/','id'=>$good->id]);?>" class="d-block">
                                    <h4><?=$good->title?></h4>
                                </a>
                                <div class="mt-3">
                                    <span class="mr-4">Цена: <strong>По запросу</strong></span>

                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                }

            }
            ?>
        </div>
    </div>
</section>
<section class="feature-area section_gap_bottom_custom pt-0">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="main_title">
                    <h2><span>Наши партнеры</span></h2>

                </div>
            </div>
        </div>
        <div class="row">
            
            <div class="col-lg-3 col-md-6">
                <div class="single-feature">
                    <a href="https://мойгазовыйкотел.рф/" target='_blank' class="title">
                        <img src="https://xn--c1aekbfedbmgce6cg8joa.xn--p1ai/static/images/rsa.jpg" alt="мойгазовыйкотел.рф" height="90"  >
                        <h3 class="text-center">Газовые котлы<br>Rossen</h3>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="single-feature">
                    <a href="https://мойугольныйкотел.рф/" target='_blank' class="title">
                        <img  src="https://xn--c1aekbfedbmgce6cg8joa.xn--p1ai/assets/images/16.jpg" alt="https://мойугольныйкотел.рф" height="90" >
                        <h3 class="text-center">Автоматизированные угольные котлы</h3>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="single-feature">
                    <a href="https://котельных-сервис.рф/" target='_blank' class="title">
                        <img  src="https://xn--80aebkijeikbwed0d0h.xn--p1ai/static/img/person.png" alt="https://котельных-сервис.рф" height="90"  >
                        <h3 class="text-center">Обслуживание котельных</h3>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="single-feature">
                    <a href="http://ecostar.ru.com/" target='_blank' class="title">
                        <img  src="http://ecostar.ru.com/Ecostar.jpg" alt="http://ecostar.ru.com/" height="90"  >
                        <h3 class="text-center">Горелки<br>Ecostar</h3>
                    </a>
                </div>
            </div>

            


        </div>
    </div>
</section>

<?php

//начало многосточной строки, можно использовать любые кавычки
$script = <<< JS
        $(function(){
        $('#carousel-765102').carousel({
            interval: 500,
            
        });
    });
JS;
//маркер конца строки, обязательно сразу, без пробелов и табуляции
$this->registerJs($script, yii\web\View::POS_READY);

?>
