<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'О НАС';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container-fluid">
    <div class="row">
        <div class=" col-xs-12 col-md-8  offset-md-2 p-2 border-dark rounded">
            <p>
                Компания <strong>ООО «КВАРЦ Энерджи»</strong> - молодая и активно развивающаяся компания в области поставок теплотехнического оборудования.
            </p>
            <p>
                Мы предлагаем широкий ассортимент запасных частей для горелочных устройств, ведущих мировых производителей представленных в рынке теплоэнергетики России.
            </p>
            <p>
                Мы работаем без посредников, для того чтобы обеспечить лучшие условия для наших партнеров.
                <br>Благодаря грамотно выстроенной логистике, мы поставляем оборудование в любой город России в короткий срок.
            </p>
            <p>
                Профессионализм нашей команды оценили сотни предприятий по всей стране.
                <br>
                <strong>ООО «КВАРЦ Энерджи»</strong> приглашает к сотрудничеству всех заинтересованных лиц.
            </p>
        </div>
    </div>
    <div class="row">
        <div class="main_title mb-0  col-xs-12 col-md-8  offset-md-2 pt-5  pb-0 border-dark rounded">
            <h2 class="text-center"><span>Мы оказываем техническую поддержку оборудованию марок:</span></h2>
        </div>
    </div>

    <div class="row p-0">
        <div class=" col-xs-12 col-md-8  offset-md-2 pb-2 pt-0 border-dark rounded">

            <div class="col-md-4">
                <a href="content/about/Ecoflame.png" class="img-gal">
                    <div class="single-gallery-image" style="background: url(content/about/Ecoflame.png);"></div>
                </a>
            </div>
            <div class="col-md-4">
                <a href="content/about/Ecostar.png" class="img-gal">
                    <div class="single-gallery-image" style="background: url(content/about/Ecostar.png);"></div>
                </a>
            </div>
            <div class="col-md-4">
                <a href="content/about/Elco.jpg" class="img-gal">
                    <div class="single-gallery-image" style="background: url(content/about/Elco.jpg);"></div>
                </a>
            </div>
            <div class="col-md-4">
                <a href="content/about/Girsh.jpg" class="img-gal">
                    <div class="single-gallery-image" style="background: url(content/about/Girsh.jpg);"></div>
                </a>
            </div>
            <div class="col-md-4">
                <a href="content/about/Hansa.jpg" class="img-gal">
                    <div class="single-gallery-image" style="background: url(content/about/Hansa.jpg);"></div>
                </a>
            </div>
            <div class="col-md-4">
                <a href="content/about/Logo-Riello.jpg" class="img-gal">
                    <div class="single-gallery-image" style="background: url(content/about/Logo-Riello.jpg);"></div>
                </a>
            </div>
            <div class="col-md-4">
                <a href="content/about/Weishaupt.png" class="img-gal">
                    <div class="single-gallery-image" style="background: url(content/about/Weishaupt.png);"></div>
                </a>
            </div>
            <div class="col-md-4">
                <a href="content/about/Wesper.gif" class="img-gal">
                    <div class="single-gallery-image" style="background: url(content/about/Wesper.gif);"></div>
                </a>
            </div>
            <div class="col-md-4">
                <a href="content/about/baltur_logo.jpg" class="img-gal">
                    <div class="single-gallery-image" style="background: url(content/about/baltur_logo.jpg);"></div>
                </a>
            </div>
            <div class="col-md-4">
                <a href="content/about/dreizler_logo.jpg" class="img-gal">
                    <div class="single-gallery-image" style="background: url(content/about/dreizler_logo.jpg);"></div>
                </a>
            </div>
            <div class="col-md-4">
                <a href="content/about/oilon-small-385x385.jpg" class="img-gal">
                    <div class="single-gallery-image" style="background: url(content/about/oilon-small-385x385.jpg);background-size:contain "></div>
                </a>
            </div>
            <div class="col-md-4">
                <a href="content/about/saacke.jpg.png" class="img-gal">
                    <div class="single-gallery-image" style="background: url(content/about/saacke.jpg.png);"></div>
                </a>
            </div>
        </div>
    </div>


    <div class="row">
        <div class=" col-xs-12 col-md-8  offset-md-2 p-2 border-dark rounded">
            <h3 class="title_color">Оборудование марки Dungs</h3>
            <p>
                Компания Karl Dungs GmbH &amp; Co. KG производитель газового оборудования основана в 1945 году, как электромонтажное предприятие. С 1970 года приступила к производству топочных автоматов и электромагнитных клапанов.
            </p>
            <p>
                Сегодня компания Karl Dungs GmbH &amp; Co. KG оснащает своим оборудованием большинство горелочных устройств применяемых во всем мире.
            </p>
            <p>
                Компания ООО «Кварц Энерджи» является торговым партнером компании ООО «Карл Дунгс» и обеспечивает продажу и техническую поддержку оборудования марки Dungs.
            </p>
            <p>
            <h3 class="title_color text-left">Сертификат официальный партнёр Dungs</h3>
            <img style='max-height:500px;max-widht:350px' src="/content/about/Dungs-1.jpg" class="image-fluid" alt="Сертификат официальный партнёр Dungs"/>
            </p>
        </div>
    </div>
        <div class="row">
            <div class=" col-xs-12 col-md-8  offset-md-2 p-2 border-dark rounded">
                <h3 class="title_color">Оборудование марки Siemens</h3>
                <p>
                    Компания Siemens AG была основана 1847 году, ее создателем является инженер и ученый — Вернер Сименс. Успех к компании Siemens пришел в 1850 году с изобретением телеграфа. Сегодня Siemens AG - крупнейший немецкий концерн работающий в области электроники, энергетического оборудования, светотехники и медицины.
                </p>
                <p>
                    Большинство горелочных устройств поставляемых в нашу страну, оснащены автоматикой управления фирмы Siemens.
                </p>
                <p>
                    Компания ООО «Кварц Энерджи» является партнером компании Siemens и обеспечивает продажу и техническую поддержку оборудования марки Siemens.
                </p>
                <p>
                <h3 class="title_color text-left">Сертификат официальный партнёр Siemens</h3>
                <img  style='max-height:500px;max-widht:350px' src="/content/about/sertSim.jpg" class="image-fluid" alt="Сертификат официальный партнёр Dungs">
                </p>
            </div>
        </div>

            <div class="row">
                <div class=" col-xs-12 col-md-8  offset-md-2 p-2 border-dark rounded">
                    <h3 class="title_color">Оборудование марки Autoflame</h3>
                    <p>
                        Компания Autoflame была основана в 1968 году, как сервисная компания в области автоматизации, котельного оборудования. Сегодня Autoflame – крупнейший производитель контроллеров, которые управляют процессами сжигания топлива.
                    </p>
                    <p>
                        Компания ООО «Кварц Энерджи» является поставщиком оборудования марки Autoflame и обеспечивает поддержку данного оборудования.
                    </p>
                </div>
            </div>
            <div class="row">
                <div class=" col-xs-12 col-md-8  offset-md-2 p-2 border-dark rounded">
                    <h3 class="title_color">Оборудование марки Suntec</h3>
                    <p>
                        Компания SUNTEC была основана в 1984 году путем поглощения компании Sundstrand которая с 1934 года производила шестеренные насосы для масляных горелок.
                    </p>
                    <p>
                        Сегодня SUNTEC разрабатывает, производит и продает непревзойденный ассортимент шестеренных насосов и аксессуаров.
                    </p>
                    <p>
                        SUNTEC, партнер крупнейших производителей систем отопления (бытовые горелки, промышленные котлы, мобильные отопительные установки и системы отопления для транспортных средств), также предлагает технические решения для всех областей применения с использованием шестеренчатого насоса.
                    </p>
                    <p>
                        Компания ООО «Кварц Энерджи» является поставщиком насосов марки SUNTEC, обеспечивает техническую поддержку.
                    </p>
                </div>
            </div>
        </div>
