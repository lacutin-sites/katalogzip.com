<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CategorySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="container">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1,
            'class'=>'form-contact contact_form'
        ],
    ]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 255, 'class' => 'form-control'])->label('Найти') ?>


    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Найти'), ['class' => 'main_btn']) ?>

    </div>

    <?php ActiveForm::end(); ?>

</div>
